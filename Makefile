CC = gcc
LD = gcc
CFLAGS = -std=c99 -Wall -Werror -pedantic -D_XOPEN_SOURCE=700 -O2
LDFLAGS = 
LIBS = -lpulse-simple -lpulse -lsndfile
OBJS = psaf-rec.o
BIN = psaf-rec

.PHONY: all clean

all: $(BIN)

$(BIN): $(OBJS)
	$(LD) $(LDFLAGS) $(LIBS) $(OBJS) -o $@

%.o: %.c
	$(CC) $(CFLAGS) $< -c -o $@

clean:
	rm -f $(OBJS)
	rm -f $(BIN)

