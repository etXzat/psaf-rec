/*
 *  psaf-rec: simplistic audio recorder using pulseaudio and libsndfile
 *  Copyright (C) 2015 Roman Sommer
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include <sys/stat.h>

#include <signal.h>

#include <getopt.h>

#include <sndfile.h>

#include <pulse/simple.h>
#include <pulse/error.h>

// BUFFER_SIZE Samples (per channel) will be buffered
#define BUFFER_SIZE 2048
// used to check cmdline parameters, maximum number of channels in pulseaudio
#define MAX_CHANNELS 32

/* Info from
 * http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html#Getopt-Long-Option-Example
 * http://freedesktop.org/software/pulseaudio/doxygen/simple.html
 * http://freedesktop.org/software/pulseaudio/doxygen/sample.html
 * http://www.mega-nerd.com/libsndfile/api.html
 */

pa_simple *pa;
pa_sample_spec ss;
static volatile bool running = true;

static void pa_test_sigint_handler(int sig) {
    running = false;
    struct sigaction act = {
	.sa_handler = SIG_DFL
    };
    sigemptyset(&act.sa_mask);
    sigaction(SIGINT, &act, NULL);
}

static void die_perror(const char *msg) {
    perror(msg);
    exit(EXIT_FAILURE);
}

static void die_fprintf(const char *msg) {
    fprintf(stderr, "%s\n", msg);
    exit(EXIT_FAILURE);
}

static void print_pa_error(FILE *stream, const char *msg, int error, bool die) {
    fprintf(stream, "%s: %s\n", msg, pa_strerror(error));
    if (die)
	exit(EXIT_FAILURE);
}

static void print_sf_error(FILE *stream, const char *msg, SNDFILE *sndfile, bool die) {
    fprintf(stream, "%s: %s\n", msg, sf_strerror(sndfile));
    if (die)
	exit(EXIT_FAILURE);
}

static void usage(const struct option *opt) {
    printf("psaf-rec: simplistic commandline audio recorder using pulsaudio and libsndfile.\n");
    printf("Recording will beginn immediately after execution, press CTRL-C to stop.\n");
    printf("Released under the GNU GPLv3+\n");
    printf("Copyright 2015 Roman Sommer\n\n");
    printf("Supported commandline parameters:\n");
    while (opt && opt->name != 0) {
	printf("  --%s\n", opt->name);
	opt++;
    }
}

void install_sigint_handler(void) {
    struct sigaction act = {
	.sa_handler = pa_test_sigint_handler,
	.sa_flags = SA_RESTART
    };
    sigemptyset(&act.sa_mask);
    sigaction(SIGINT, &act, NULL);
}

int main (int argc, char **argv) {
    int samplerate=44100, channels=1;
    // redundant but makes things easier:
    int bits_per_sample = 16;

    static char *output_file;
    bool overwrite = false;

    SNDFILE *out;
    int sf_format = SF_FORMAT_FLAC | SF_FORMAT_PCM_16;
    int pa_sample_format = PA_SAMPLE_S16NE;

// parse commandline options
    int c;
    while (1) {
	static struct option long_options[] = {
	    {"bitdepth", required_argument, NULL, 'b'},
	    {"channels", required_argument, NULL, 'c'},
	    {"format", required_argument, NULL, 'f'},
	    {"output-file", required_argument, NULL, 'o'},
	    {"samplerate", required_argument, NULL, 'r'},
	    {"overwrite", no_argument, NULL , 1},
	    {"help", no_argument, NULL, 'h'},
	    {0, 0, 0, 0}
	};
	int option_index = 0;
	long tmp_sr, tmp_bd, tmp_ch;
	c = getopt_long(argc, argv, "ho:r:f:c:b:", long_options, &option_index);
	if (c == -1)
	    break;
	switch (c) {
	case 0:
	    break;
	case 'h':
	    usage(long_options);
	    exit(EXIT_SUCCESS);
	case 'b':
	    tmp_bd = atoi(optarg);
	    switch (tmp_bd) {
		sf_format &= 0xffff0000;
	    case 16:
		sf_format |= SF_FORMAT_PCM_16;
		pa_sample_format = PA_SAMPLE_S16NE;
		bits_per_sample = 16;
		break;
	    case 24:
		sf_format |= SF_FORMAT_PCM_24;
		pa_sample_format = PA_SAMPLE_S24NE;
		bits_per_sample = 24;
		break;
	    }
	    break;
	case 'c':
	    tmp_ch = strtol(optarg, NULL, 10);
	    if (tmp_ch == LONG_MIN || tmp_ch == LONG_MAX)
		die_perror("Number of channels:");
	    if (tmp_ch <= 0 || tmp_ch > MAX_CHANNELS) {
		die_fprintf("Number of channels: invalid value\n");
	    }
	    channels = (int)tmp_ch;
	    break;
	case 'o':
	    output_file = strdup(optarg);
	    if (!output_file)
		die_perror("strdup");
	    break;
	case 'r':
	    tmp_sr = strtol(optarg, NULL, 10);
	    if (tmp_sr == LONG_MIN || tmp_sr == LONG_MAX)
		die_perror("samplerate:");
	    if (tmp_sr <= 0 || tmp_sr > INT_MAX) {
		die_fprintf("samplerate: invalid value\n");
	    }
	    samplerate = (int)tmp_sr;
	    break;
	case 'f':
		sf_format &= 0xffff;
	    if (strcmp("flac", optarg) == 0) {
		sf_format |= SF_FORMAT_FLAC;
	    } else if (strcmp("vorbis", optarg) == 0) {
		sf_format |= SF_FORMAT_VORBIS;
	    } else {
		die_fprintf("unknown format");
	    }
	    break;
	case 1: // overwrite
	    overwrite = true;
	}

    }
    
    sf_format |= SF_ENDIAN_CPU;

    // open soundfile
    
    if (output_file) {
	struct stat stat;
	int stat_ret = lstat(output_file, &stat);
	int errno_bak = errno;
	if (stat_ret == 0 && !overwrite) {
	    die_fprintf("File exists, specify --overwrite if that's what you want.");
	} else if ((stat_ret != 0) && (errno_bak != ENOENT)) {
	    die_perror("output-file");
	} else if ((stat_ret == 0) && overwrite) {
	    if (!S_ISREG(stat.st_mode)) {
		die_fprintf("output-file is not a regular file");
	    }
	}
	
    } else {
	die_fprintf("You need to specify an output-file (for now).");
    }

    SF_INFO sf_info = {
	.samplerate = samplerate,
	.channels = channels,
	.format = sf_format
    };
    out = sf_open(output_file, SFM_WRITE, &sf_info );
    if (!out) {
	print_sf_error(stderr, "sf_open", NULL, true);
    }

    // open pulseaudio
    
    int pa_err;
   
    ss.rate = samplerate;
    ss.channels = channels;
    ss.format = pa_sample_format;


    pa = pa_simple_new(NULL,               // Use the default server.
		       argv[0],           // Our application's name.
		       PA_STREAM_RECORD,
		       NULL,               // Use the default device.
		       "Record",            // Description of our stream.
		       &ss,                // Our sample format.
		       NULL,               // Use default channel map
		       NULL,               // Use default buffering attributes.
		       &pa_err               // Ignore error code.
	);
    if (!pa) {
	print_pa_error(stderr, "pa_simple_new()", pa_err, false);
	sf_close(out);
	exit(EXIT_FAILURE);
    }

    // prepare copy loop
    
    install_sigint_handler();
    size_t buffer_size = BUFFER_SIZE * (bits_per_sample / 8) * channels;
    char buf[buffer_size];

    // copy loop
    
    while (running) {
	// abort with sigint
	if (pa_simple_read(pa, buf, buffer_size, &pa_err) < 0) {
	    print_pa_error(stderr, "pa_simple_read()", pa_err, false);
	    break;
	}

	sf_count_t sf_items = BUFFER_SIZE * channels;
	char *read_pointer = buf;
	if (bits_per_sample == 16) {
	    while (sf_items > 0) {
		sf_count_t sf_items_written = sf_write_short(out,
							  (short*)read_pointer,
							  sf_items);
		sf_items -= sf_items_written;
		read_pointer += sf_items * channels * (bits_per_sample / 8);
	    }
	} else if (bits_per_sample == 24) {
	    // TODO
	    break;
	} else {
	    // TODO
	    break;
	}
    }

    printf("\nClosing file.\n");
    sf_close(out);
    pa_simple_free(pa);
    exit(EXIT_SUCCESS);
}
